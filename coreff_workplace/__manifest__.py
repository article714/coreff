# -*- coding: utf-8 -*-
# @author: J. Carette
# @copyright: ©2018 Article 714
# @license: AGPL v3

{
    'name': u'CoreFF workplace',
    'version': u'10.0.1.0.0',
    'category': u'CoreFF',
    'author': u'Article714',
    'license': u'AGPL-3',
    'website': u'https://www.article714.org',
    'description': u"""
CoreFF -  CoreFF workplace
=================================

CoreFF Workplace


**Credits:** Article714.
""",
    'depends': ['coreff_base','coreff_creditsafe','coreff_societecom','coreff_informa'],
    'data': [
             ],
    'installable': True,
    'images': [],
    'application': True,
}
