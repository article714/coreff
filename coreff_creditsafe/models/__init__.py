# -*- coding: utf-8 -*-
'''
Created on 8 August 2018

@author: J. Carette
@copyright: ©2018 Article 714
@license: AGPL v3
'''

from . import coreff_partner
from . import coreff_config
from . import coreff_create_from_wizard

